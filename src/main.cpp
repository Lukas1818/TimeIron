#include <Arduino.h>
#include "TimeIron.h"

TimeIron t;

void setup() {
  Serial.begin(9600);
  t.reset();
}

void loop() {
  int s =Serial.read();
  if (s=='g')
  {
    Serial.print("milli: ");
    Serial.println(t.getMillis());
    Serial.print("micro: ");
    Serial.println(t.getMicros());
  }
  if (s=='r')
  {
    t.reset();
    Serial.println("Reset");
  }
}
