#include <Arduino.h>
#include "TimeIron.h"


void TimeIron::reset()
{
  mic=micros();
  mill=millis();
}


unsigned long TimeIron::getMicros()
{
  unsigned long i = micros();
  if (i>mic) return i-mic;
  return 4294967295 - (mic-i);
}

unsigned long TimeIron::getMillis()
{
  unsigned long i = millis();
  if (i>mill) return i-mill;
  return 4294967295 - (mill-i);
}
