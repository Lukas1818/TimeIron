#pragma once

class TimeIron
{
public:
  void reset();
  unsigned long getMicros();
  unsigned long getMillis();
private:
  unsigned long mic;
  unsigned long mill;
};
